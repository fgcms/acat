package com.mango.acat.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)//类注解
@Retention(RetentionPolicy.RUNTIME)//运行时环境
public @interface ReqKey {
	public String value();//请求的key,类似spring mvc的requestMapping
}
