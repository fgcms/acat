package com.mango.acat.core.util;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringKit {
	private static ClassPathXmlApplicationContext context;
	
	public static void start(String[] config){
		context = new ClassPathXmlApplicationContext(config);
		context.start();
	}
	
	public static Object getBean(String name){
		return context.getBean(name);
	}
}
