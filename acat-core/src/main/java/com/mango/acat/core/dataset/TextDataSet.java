package com.mango.acat.core.dataset;

public class TextDataSet extends DataSet{
	private String value;
	
	public TextDataSet(String value) {
		this.setType(TEXT);
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
