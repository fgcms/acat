package com.mango.acat.core.util;

import com.mango.acat.core.setting.AppSetting;

import io.netty.util.internal.SystemPropertyUtil;

/**
 * 路径工具
 * @author Administrator
 *
 */
public class PathKit {
	
	private static String relativePath = AppSetting.app.getStr("app.content.path","");
	/**
	 * 获取项目启动服务器带相对路径跟路径，如开发环境src\main\webapp
	 * @return
	 */
	public static String getRealRootPath(){
		return getRealPath() + relativePath;
	}
	/**
	 * 获取项目启动服务器根路径
	 * @return
	 */
	public static String getRealPath(){
		return SystemPropertyUtil.get("user.dir");
	}
}
