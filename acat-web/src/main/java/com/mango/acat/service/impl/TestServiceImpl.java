package com.mango.acat.service.impl;

import java.util.List;

import org.rex.DB;
import org.rex.RMap;
import org.rex.db.exception.DBException;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.google.common.collect.Lists;
import com.mango.acat.service.TestService;

import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.CacheObject;

public class TestServiceImpl implements TestService{
	private static CacheChannel cache = CacheChannel.getInstance();

	@SuppressWarnings("rawtypes")
	public List<RMap> getList() {
		List<RMap> list = null;
		try {
			CacheObject obj  = cache.get("db", "listids");
			if(obj.getValue() != null){
				System.out.println("从缓存获取db list,"+obj);
				String sql = "select * from cms_article ";
				sql += " where id in ("+obj.getValue()+")";
				list = DB.getMapList(sql);
			}else{
				String sql = "SELECT * FROM cms_article";
		        list = DB.getMapList(sql,1,10);
		        List<String> ids = Lists.newArrayList();
		        for(RMap rm : list){
		        	ids.add("'"+rm.getString("id")+"'");
		        }
		        cache.set("db", "listids",StringUtils.join(ids, ","));
		        System.out.println("设置缓存db list,value="+StringUtils.join(ids, ","));
			}	
		} catch (DBException e) {
			e.printStackTrace();
		}
		return list;
	}
	

}
