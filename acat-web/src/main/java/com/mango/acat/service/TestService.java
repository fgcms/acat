package com.mango.acat.service;

import java.util.List;

import org.rex.RMap;

public interface TestService {
	
	@SuppressWarnings("rawtypes")
	public List<RMap> getList();		
}
